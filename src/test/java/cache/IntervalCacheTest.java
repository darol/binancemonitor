package cache;

import component.Interval;
import org.awaitility.Awaitility;
import org.junit.Test;

import java.util.*;

public class IntervalCacheTest {
    private int counter = 1;

    @Test
    public void getCacheData() {
        IntervalCache<Integer> intervalListCache = new IntervalCache<>(Interval.HUNDRED_MILLIS, () -> counter--);

        Awaitility.await().atMost(org.awaitility.Duration.TWO_HUNDRED_MILLISECONDS)
                .until(() -> intervalListCache.get().orElse(0) == 1);

        Awaitility.await().atMost(org.awaitility.Duration.TWO_HUNDRED_MILLISECONDS)
                .until(() -> intervalListCache.get().orElse(0) == 0);
    }
}