package client;

import org.junit.Assert;
import org.junit.Test;

public class BinanceExchangeClientTest {

    private final BinanceExchangeClient binanceExchangeClient = new BinanceExchangeClient();

    @Test
    public void getServerTime() {
        Assert.assertTrue(binanceExchangeClient.getServerTime() > 0);
    }
}