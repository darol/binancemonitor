package client;

import net.sealake.binance.api.client.domain.market.TickerPrice;
import net.sealake.binance.api.client.domain.market.TickerStatistics;

import java.util.Collections;
import java.util.List;

public class TestExchangeClient implements ExchangeClient{

    @Override
    public Long getServerTime() {
        return 1L;
    }

    @Override
    public List<TickerPrice> getAllPrices() {
        TickerPrice tickerPrice = new TickerPrice();
        tickerPrice.setSymbol(TestExchange.SYMBOL_WITH_BTC);
        tickerPrice.setPrice(String.valueOf(TestExchange.PRICE));
        return Collections.singletonList(tickerPrice);
    }

    @Override
    public List<TickerStatistics> getAll24HrPriceStatistics() {
        TickerStatistics tickerStatistics = new TickerStatistics();
        tickerStatistics.setSymbol(TestExchange.SYMBOL_WITH_BTC);
        tickerStatistics.setVolume(String.valueOf(TestExchange.VOLUME));
        return Collections.singletonList(tickerStatistics);
    }
}
