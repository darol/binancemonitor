package client;

import component.DefinedSymbol;

public class TestExchange {
    public static final Double PRICE = 1.01;
    public static final Double VOLUME = 500.2;
    public static final String SYMBOL = "SMB";
    public static final String SYMBOL_WITH_BTC = SYMBOL + DefinedSymbol.BTC.toString();

    public static final ExchangeClient TEST_EXCHANGE_CLIENT = new TestExchangeClient();
}
