package provider;

import client.TestExchange;
import org.junit.Assert;
import org.junit.Test;

public class PriceProviderTest {

    @Test
    public void getPrice() {
        PriceProvider priceProvider = new PriceProvider(TestExchange.TEST_EXCHANGE_CLIENT);

        Assert.assertEquals(TestExchange.PRICE, priceProvider.get(TestExchange.SYMBOL_WITH_BTC));
    }
}