package client;

import net.sealake.binance.api.client.domain.market.TickerPrice;
import net.sealake.binance.api.client.domain.market.TickerStatistics;

import java.util.List;

public interface ExchangeClient {
    Long getServerTime();
    List<TickerPrice> getAllPrices();
    List<TickerStatistics> getAll24HrPriceStatistics();
}
