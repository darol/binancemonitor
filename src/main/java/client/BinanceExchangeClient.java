package client;

import cache.IntervalCache;
import component.Interval;
import net.sealake.binance.api.client.BinanceApiClientFactory;
import net.sealake.binance.api.client.BinanceApiRestClient;
import net.sealake.binance.api.client.domain.market.TickerPrice;
import net.sealake.binance.api.client.domain.market.TickerStatistics;

import java.util.ArrayList;
import java.util.List;

public class BinanceExchangeClient implements ExchangeClient{
    private final BinanceApiRestClient client = BinanceApiClientFactory.newInstance().newRestClient();

    private final IntervalCache<List<TickerStatistics>> tickerStatisticsCache = new IntervalCache<>(Interval.ONE_MINUTE, client::getAll24HrPriceStatistics);
    private final IntervalCache<List<TickerPrice>> tickerPriceCache = new IntervalCache<>(Interval.FIVE_SECONDS, client::getAllPrices);

    @Override
    public Long getServerTime(){
        return client.getServerTime();
    }

    @Override
    public List<TickerPrice> getAllPrices(){
        return tickerPriceCache.get().orElse(new ArrayList<>());
    }

    @Override
    public List<TickerStatistics> getAll24HrPriceStatistics() {
        return tickerStatisticsCache.get().orElse(new ArrayList<>());
    }
}
