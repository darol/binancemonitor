package component;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public enum Interval {
    HUNDRED_MILLIS(TimeUnit.MILLISECONDS.toMillis(100), "100ms", false),
    FIVE_SECONDS(TimeUnit.SECONDS.toMillis(5), "5s", false),
    ONE_MINUTE(TimeUnit.MINUTES.toMillis(1), "1m", true),
    FIVE_MINUTES(TimeUnit.MINUTES.toMillis(5), "5m", true),
    FIFTEEN_MINUTES(TimeUnit.MINUTES.toMillis(15), "15m", true),
    THIRTY_MINUTES(TimeUnit.MINUTES.toMillis(30), "30m", true),
    ONE_HOUR(TimeUnit.HOURS.toMillis(1), "1h", true),
    TWO_HOURS(TimeUnit.HOURS.toMillis(2), "2h", true),
    TWENTY_FOUR_HOURS(TimeUnit.HOURS.toMillis(24), "24h", false);

    private final long millis;
    private final String name;
    private final boolean visible;

    Interval(long millis, String name, boolean visible) {
        this.millis = millis;
        this.name = name;
        this.visible = visible;
    }

    public long getMillis() {
        return millis;
    }

    public String getName() {
        return name;
    }

    public boolean isVisible() {
        return visible;
    }

    public static Interval getByString(String s){
        return Arrays.stream(values())
                .filter(interval -> interval.name.equals(s))
                .findFirst()
                .orElse(Interval.ONE_MINUTE);
    }
}
