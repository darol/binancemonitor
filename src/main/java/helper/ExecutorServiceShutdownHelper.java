package helper;

import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorServiceShutdownHelper {
    private static final Logger LOGGER = Logger.getLogger(ExecutorServiceShutdownHelper.class);
    private final static long TIMEOUT_SECONDS = TimeUnit.SECONDS.toSeconds(2);

    public static void shutdownExecutorService(ExecutorService executorService, boolean awaitTermination){
        if(isExecutorActive(executorService)){
            new Thread(() -> shutdownExecutor(executorService, awaitTermination)).start();
        }
    }

    public static boolean isExecutorActive(ExecutorService executorService){
        return executorService != null && !executorService.isShutdown() && !executorService.isTerminated();
    }

    private static void shutdownExecutor(ExecutorService executorService, boolean awaitTermination) {
        try {
            shutdown(executorService);
            awaitTermination(executorService, awaitTermination);
            shutdownNow(executorService);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }

    private static void shutdown(ExecutorService executorService) {
        LOGGER.info("Shutting down executor");
        executorService.shutdown();
    }

    private static void awaitTermination(ExecutorService executorService, boolean awaitTermination) throws InterruptedException {
        if(awaitTermination && !executorService.isTerminated()){
            LOGGER.info(String.format("Awaiting %s seconds for executor termination", TIMEOUT_SECONDS));
            executorService.awaitTermination(TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
    }

    private static void shutdownNow(ExecutorService executorService) {
        if(!executorService.isTerminated()){
            LOGGER.info("Executor not terminated, shut down now");
            executorService.shutdownNow();
        }else{
            LOGGER.info("Executor shut down");
        }
    }
}
