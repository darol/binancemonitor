package helper;

public class ParsingHelper {
    public static Double getDouble(String s, Double defaultValue){
        try{
            return Double.parseDouble(s);
        }catch (NumberFormatException e){
            return defaultValue;
        }
    }
}
