package monitor;

import component.Interval;
import helper.ExecutorServiceShutdownHelper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class MonitorRunnableExecutor {
    private ExecutorService executorService;
    private final Interval interval;

    public MonitorRunnableExecutor(Interval interval) {
        this.interval = interval;
    }

    protected abstract Runnable getRunnable();

    public void start(){
        if(!ExecutorServiceShutdownHelper.isExecutorActive(executorService)){
            executorService = Executors.newScheduledThreadPool(1);
            ((ScheduledExecutorService) executorService).scheduleAtFixedRate(getRunnable(), 0, interval.getMillis(), TimeUnit.MILLISECONDS);
        }
    }

    public void stop(){
        if(ExecutorServiceShutdownHelper.isExecutorActive(executorService)){
            ExecutorServiceShutdownHelper.shutdownExecutorService(executorService, true);
        }
    }
}
