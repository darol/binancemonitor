package fxml;

public class FilterInfo {
    private boolean selected = false;
    private double volume = 0;
    private double price = 0;

    public FilterInfo withPrice(double price){
        this.price = price;
        return this;
    }

    public FilterInfo withVolume(double volume){
        this.volume = volume;
        return this;
    }

    public FilterInfo withSelected(boolean selected){
        this.selected = selected;
        return this;
    }

    public boolean isSelected() {
        return selected;
    }

    public double getVolume() {
        return volume;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "FilterInfo{" +
                "selected=" + selected +
                ", volume=" + volume +
                ", price=" + price +
                '}';
    }
}
