package fxml;

import component.Interval;

public class TokenInfo {
    private final String name;
    private Double price = 0d;
    private Double volume = 0d;

    public TokenInfo(String name) {
        this.name = name;
    }

    public void set(Double price, Double volume){
        this.price = price;
        this.volume = volume * price;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public Double getVolume() {
        return volume;
    }

    public String getPercent(Interval interval, ColumnType columnType){
        // TODO use provider here
        return String.format("%.2f", Math.random() > 0.5 ? -Math.random(): Math.random());
    }
}
