package fxml.controller;

import component.DefinedSymbol;
import component.Interval;
import fxml.ColumnType;
import fxml.TokenInfo;
import fxml.FilterInfo;
import helper.ParsingHelper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import provider.Provider;

import java.math.BigDecimal;
import java.math.MathContext;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class MonitorController implements Initializable {

    @FXML
    private TextField lblVolume;

    @FXML
    private ToggleButton toggleFilter;

    @FXML
    private HBox filterGroup;

    @FXML
    private TableView<TokenInfo> tokenTable;

    @FXML
    private TextField lblPrice;

    @FXML
    private HBox toggleGroup;

    @FXML
    private HBox intervalGroup;

    @FXML
    private TableColumn<TokenInfo, String> columnVolatility;

    @FXML
    private TableColumn<TokenInfo, String> columnChange;

    private static final int SATOSHI_PRECISION = 8;

    private final Map<String, FilterInfo> filterCache = new HashMap<>();
    private final ObservableList<TokenInfo> tokenList = FXCollections.observableArrayList();
    private final ObservableList<TokenInfo> filteredTokenList = FXCollections.observableArrayList();

    private final Collection<Interval> intervals = Arrays.stream(Interval.values())
            .filter(Interval::isVisible)
            .collect(Collectors.toList());

    private final List<ToggleButton> symbolToggles = Arrays.stream(DefinedSymbol.values())
            .map(definedSymbol -> new ToggleButton(definedSymbol.name()))
            .collect(Collectors.toList());

    private final List<ToggleButton> intervalToggles = intervals.stream()
            .map(interval -> new ToggleButton(interval.getName()))
            .collect(Collectors.toList());

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        prepareSymbolToggles();
        prepareIntervalToggles();
        prepareTokenFilter();
        prepareTokenTable();
    }

    private void prepareSymbolToggles() {
        ToggleGroup symbolGroup = new ToggleGroup();
        symbolToggles.forEach(button -> button.setToggleGroup(symbolGroup));
        symbolToggles.forEach(button -> button.setOnAction(event -> {
            symbolChanged(getSelectedSymbol());
        }));
        symbolToggles.forEach(button -> toggleGroup.getChildren().add(button));
    }

    private void prepareIntervalToggles() {
        intervalToggles.forEach(button -> button.setOnAction(event -> {
            refreshIntervals();
        }));
        intervalToggles.forEach(button -> intervalGroup.getChildren().add(button));
    }

    private void prepareTokenFilter() {
        toggleFilter.selectedProperty().addListener((observable, oldValue, newValue) ->
                getFilterInfo().withSelected(newValue));
        lblPrice.textProperty().addListener((observable, oldValue, newValue) ->
                getFilterInfo().withPrice(ParsingHelper.getDouble(newValue, 0d)));
        lblVolume.textProperty().addListener((observable, oldValue, newValue) ->
                getFilterInfo().withVolume(ParsingHelper.getDouble(newValue, 0d)));
    }

    private void prepareTokenTable() {
        int idx = 0;

        TableColumn<TokenInfo, String> nameColumn = new TableColumn<>("Name");
        TableColumn<TokenInfo, Integer> volumeColumn = new TableColumn<>("Volume");
        TableColumn<TokenInfo, String> priceColumn = new TableColumn<>("Price");

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        priceColumn.setCellValueFactory(param -> new SimpleStringProperty(new BigDecimal(param.getValue().getPrice())
                .round(new MathContext(SATOSHI_PRECISION)).stripTrailingZeros().toPlainString()));
        volumeColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getVolume().intValue()).asObject());

        tokenTable.getColumns().add(idx++, nameColumn);
        tokenTable.getColumns().add(idx++, volumeColumn);
        tokenTable.getColumns().add(idx, priceColumn);

        refreshIntervals();

        tokenTable.setItems(filteredTokenList);
    }

    private void refreshIntervals() {
        refreshColumnIntervals(ColumnType.CHANGE);
        refreshColumnIntervals(ColumnType.VOLATILITY);
    }

    private void refreshColumnIntervals(ColumnType columnType) {
        TableColumn<TokenInfo, String> column = columnType == ColumnType.CHANGE ? columnChange : columnVolatility;

        column.getColumns().clear();

        intervalToggles.stream()
                .filter(ToggleButton::isSelected)
                .map(toggleButton -> Interval.getByString(toggleButton.getText()))
                .forEach(interval -> addIntervalToParentColumn(interval, column, columnType));

        column.setVisible(!column.getColumns().isEmpty());
    }

    private void addIntervalToParentColumn(Interval interval, TableColumn<TokenInfo, String> parent, ColumnType columnType) {
        TableColumn<TokenInfo, String> column = new TableColumn<>(interval.getName());

        column.setPrefWidth(70); // without it width is messed up
        column.setStyle( "-fx-alignment: CENTER;");
        column.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getPercent(interval, columnType)));
        column.setCellFactory(new Callback<TableColumn<TokenInfo, String>, TableCell<TokenInfo, String>>() {
            @Override
            public TableCell<TokenInfo, String> call(TableColumn<TokenInfo, String> param) {
                return new TableCell<TokenInfo, String>() {

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!isEmpty()) {
                            setTextFill(item.startsWith("-") ? Color.RED : Color.GREEN);
                            setText(item);
                        }
                    }
                };
            }
        });

        parent.getColumns().add(column);
    }

    private void symbolChanged(String selectedSymbol) {
        refreshTokenList(selectedSymbol);
        refreshFilteredTokenList();
        refreshFilterPanel(selectedSymbol, getFilterInfo());
    }

    private void refreshTokenList(String selectedSymbol) {
        tokenList.clear();
        if (!selectedSymbol.isEmpty()) {
            List<TokenInfo> tokens = Provider.SYMBOL.getAll().stream()
                    .filter(s -> s.endsWith(selectedSymbol))
                    .map(TokenInfo::new)
                    .collect(Collectors.toList());

            tokens.forEach(this::updateTokenInfo);

            tokenList.addAll(tokens);
        }
    }

    private FilterInfo getFilterInfo() {
        return filterCache.computeIfAbsent(getSelectedSymbol(), k -> new FilterInfo());
    }

    private void refreshFilterPanel(String selectedSymbol, FilterInfo filterInfo) {
        toggleFilter.setSelected(filterInfo.isSelected());
        lblPrice.setText(String.valueOf(filterInfo.getPrice()));
        lblVolume.setText(String.valueOf(filterInfo.getVolume()));

        filterGroup.setDisable(selectedSymbol.isEmpty());
    }

    private void refreshFilteredTokenList() {
        filteredTokenList.clear();
        filteredTokenList.addAll(tokenList.stream()
                .filter(this::applyFilter)
                .collect(Collectors.toList()));
    }

    private boolean applyFilter(TokenInfo tokenInfo) {
        FilterInfo filterInfo = getFilterInfo();
        return !filterInfo.isSelected() || tokenInfo.getPrice() >= filterInfo.getPrice() &&
                tokenInfo.getVolume() >= filterInfo.getVolume();
    }

    private void updateTokenInfo(TokenInfo tokenInfo) {
        tokenInfo.set(Provider.PRICE.get(tokenInfo.getName()), Provider.VOLUME.get(tokenInfo.getName()));
    }

    private String getSelectedSymbol() {
        return symbolToggles.stream()
                .filter(ToggleButton::isSelected)
                .findFirst().orElse(new ToggleButton(""))
                .getText();
    }

    @FXML
    void btnRefresh(ActionEvent event) {
        tokenTable.refresh();
    }

    @FXML
    void btnFilter(ActionEvent event) {
        refreshFilteredTokenList();
    }
}
