package cache;

import component.Interval;
import org.joda.time.DateTime;

import java.util.Optional;

public class IntervalCache<T> {
    private CacheDataProvider<T> cacheDataProvider;
    private final Interval refreshInterval;
    private DateTime lastUpdate = new DateTime(0);
    private T data;

    public IntervalCache(Interval refreshInterval, CacheDataProvider<T> cacheDataProvider) {
        this.refreshInterval = refreshInterval;
        this.cacheDataProvider = cacheDataProvider;
    }

    private boolean hasLatestData(){
        return DateTime.now().minusMillis((int) refreshInterval.getMillis()).isBefore(lastUpdate);
    }

    public Optional<T> get(){
        if(!hasLatestData()){
            data = cacheDataProvider.get();
            lastUpdate = DateTime.now();
        }

        return Optional.of(data);
    }
}
