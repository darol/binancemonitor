package cache;

public interface CacheDataProvider<T> {
    T get();
}
