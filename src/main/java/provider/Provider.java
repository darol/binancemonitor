package provider;

import client.BinanceExchangeClient;
import client.ExchangeClient;

public class Provider {
    private static final ExchangeClient EXCHANGE_CLIENT = new BinanceExchangeClient();

    public static final PriceProvider PRICE = new PriceProvider(EXCHANGE_CLIENT);
    public static final VolumeProvider VOLUME = new VolumeProvider(EXCHANGE_CLIENT);
    public static final SymbolProvider SYMBOL = new SymbolProvider(EXCHANGE_CLIENT);
}
