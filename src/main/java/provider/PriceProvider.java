package provider;

import cache.IntervalCache;
import component.Interval;
import client.ExchangeClient;
import helper.ParsingHelper;
import net.sealake.binance.api.client.domain.market.TickerPrice;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class PriceProvider {
    private final ExchangeClient client;
    private final IntervalCache<Map<String, Double>> cache = new IntervalCache<>(Interval.FIVE_SECONDS, this::getPrices);

    PriceProvider(ExchangeClient client) {
        this.client = client;
    }

    public Double get(String symbol) {
        return cache.get().orElse(new HashMap<>()).getOrDefault(symbol, 0d);
    }

    private Map<String, Double> getPrices() {
        return client.getAllPrices().stream()
                .collect(Collectors.toMap(
                        TickerPrice::getSymbol,
                        o -> ParsingHelper.getDouble(o.getPrice(), 0d)));
    }
}
