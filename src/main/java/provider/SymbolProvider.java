package provider;

import cache.IntervalCache;
import client.ExchangeClient;
import component.Interval;
import net.sealake.binance.api.client.domain.market.TickerPrice;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SymbolProvider {
    private final ExchangeClient client;
    private final IntervalCache<List<String>> cache = new IntervalCache<>(Interval.ONE_HOUR, this::getAllSymbols);

    SymbolProvider(ExchangeClient client) {
        this.client = client;
    }

    public List<String> getAll(){
        return cache.get().orElse(new ArrayList<>());
    }

    private List<String> getAllSymbols() {
        return client.getAllPrices().stream()
                .map(TickerPrice::getSymbol)
                .collect(Collectors.toList());
    }
}
