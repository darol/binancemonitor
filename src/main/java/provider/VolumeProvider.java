package provider;

import cache.IntervalCache;
import component.Interval;
import client.ExchangeClient;
import helper.ParsingHelper;
import net.sealake.binance.api.client.domain.market.TickerStatistics;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class VolumeProvider {
    private final ExchangeClient client;
    private final IntervalCache<Map<String, Double>> cache = new IntervalCache<>(Interval.ONE_MINUTE, this::getVolumes);

    VolumeProvider(ExchangeClient client) {
        this.client = client;
    }

    public Double get(String symbol) {
        return cache.get().orElse(new HashMap<>()).getOrDefault(symbol, 0d);
    }

    private Map<String, Double> getVolumes() {
        return client.getAll24HrPriceStatistics().stream()
                .collect(Collectors.toMap(
                        TickerStatistics::getSymbol,
                        o -> ParsingHelper.getDouble(o.getVolume(), 0d)));
    }
}
